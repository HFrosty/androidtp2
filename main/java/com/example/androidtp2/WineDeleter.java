package com.example.androidtp2;

import android.content.Context;
import android.os.Debug;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

public class WineDeleter implements View.OnCreateContextMenuListener {

    private Context context;
    private long wineId;
    private MainActivity main;

    public WineDeleter(Context context, MainActivity main)
    {
        this.context = context;
        this.main = main;
        wineId = -1;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = new MenuInflater(context);
        inflater.inflate(R.menu.context_menu, menu);

        MenuItem tv = menu.findItem(R.id.delete);
        tv.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                deleteTarget();
                return false;
            }
        });
    }

    public void setWineId(long id)
    {
        this.wineId = id;
        System.out.println("Deleter target is now wine n°:" + id);
    }

    public void deleteTarget() {
        WineDbHelper db = new WineDbHelper(context);
        db.deleteWine(db.idToCursor(wineId));
        main.resetData();
    }

}
