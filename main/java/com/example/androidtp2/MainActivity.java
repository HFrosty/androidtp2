package com.example.androidtp2;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private WineDeleter deleter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        deleter = new WineDeleter(getBaseContext(), this);

        final WineDbHelper db = new WineDbHelper(findViewById(R.id.wineList).getContext());

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cursor cursor = db.fetchAllWines();
                cursor.moveToLast();
                long newWineId = 0;
                if(cursor.getCount() > 0)
                {
                    newWineId = cursor.getLong(cursor.getColumnIndex(WineDbHelper._ID)) + 1;
                }
                GoToWineActivity(newWineId, true);
            }
        });
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        final ListView wineList = (ListView) findViewById(R.id.wineList);

        resetData();

        // Set what happens when user clicks on data
        wineList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                GoToWineActivity(Integer.valueOf( ((HashMap<String, String>) wineList.getItemAtPosition(i)).get("Id") ), false);
            }
        });

        wineList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                deleter.setWineId(Integer.valueOf( ((HashMap<String, String>) wineList.getItemAtPosition(position)).get("Id") ));
                return false;
            }
        });

        // Set what happens when user long clicks on data
        registerForContextMenu(wineList);
        wineList.setOnCreateContextMenuListener(deleter);
    }

    public void GoToWineActivity(long wineId, boolean newOrNot)
    {
        Intent intent = new Intent(MainActivity.this, WineActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean("New", newOrNot);
        bundle.putInt("Id", (int)wineId);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void resetData()
    {
        final ListView wineList = (ListView) findViewById(R.id.wineList);

        // Database testing
        WineDbHelper dbHelper = new WineDbHelper(wineList.getContext());
        Cursor currentWine = dbHelper.fetchAllWines();

        // Fill list data
        List<Map<String, String>> data = new ArrayList<Map<String, String>>();
        while(!currentWine.isAfterLast())
        {
            Map<String, String> element = new HashMap<String, String>(3);
            element.put("Id", Long.toString(currentWine.getLong(currentWine.getColumnIndex(WineDbHelper._ID))));
            element.put("Name", currentWine.getString(1));
            element.put("Origin", currentWine.getString(2));
            data.add(element);
            currentWine.moveToNext();
        }

        // Display data on list
        SimpleAdapter adapter = new SimpleAdapter(this, data, android.R.layout.simple_list_item_2, new String[] {"Name", "Origin"}, new int[] {android.R.id.text1, android.R.id.text2});
        wineList.setAdapter(adapter);

        adapter.notifyDataSetChanged();
    }
}
