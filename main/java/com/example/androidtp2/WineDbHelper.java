package com.example.androidtp2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class WineDbHelper extends SQLiteOpenHelper {

    private static final String TAG = WineDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 5;

    public static final String DATABASE_NAME = "wine.db";

    public static final String TABLE_NAME = "cellar";

    public static final String _ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_WINE_REGION = "region";
    public static final String COLUMN_LOC = "localization";
    public static final String COLUMN_CLIMATE = "climate";
    public static final String COLUMN_PLANTED_AREA = "publisher";

    public WineDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
	    db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_NAME + " TEXT NOT NULL," + COLUMN_WINE_REGION + " TEXT NOT NULL," +COLUMN_LOC + " TEXT NOT NULL," +COLUMN_CLIMATE + " TEXT NOT NULL," +COLUMN_PLANTED_AREA + " TEXT NOT NULL, UNIQUE (" + COLUMN_NAME + ", " + COLUMN_WINE_REGION + ") ON CONFLICT ROLLBACK);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }


   /**
     * Adds a new wine
     * @return  true if the wine was added to the table ; false otherwise (case when the pair (name, region) is
     * already in the data base
     */
    public boolean addWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();

        // Inserting Row
        long rowID = 0;
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, wine.getTitle());
        values.put(COLUMN_WINE_REGION, wine.getRegion());
        values.put(COLUMN_LOC, wine.getLocalization());
        values.put(COLUMN_CLIMATE, wine.getClimate());
        values.put(COLUMN_PLANTED_AREA, wine.getPlantedArea());
        rowID = db.insert(TABLE_NAME, null, values);
        db.close(); // Closing database connection

        return (rowID != -1);
    }

    /**
     * Updates the information of a wine inside the data base
     * @return the number of updated rows
     */
    public int updateWine(Wine wine) {
        System.out.println("NEW WINE ADDED");
        SQLiteDatabase db = this.getWritableDatabase();
	    int res = -1;

        // updating row
        ContentValues values = new ContentValues();
        values.put(COLUMN_CLIMATE, wine.getClimate());
        values.put(COLUMN_LOC, wine.getLocalization());
        values.put(COLUMN_NAME, wine.getTitle());
        values.put(COLUMN_PLANTED_AREA, wine.getPlantedArea());
        values.put(COLUMN_WINE_REGION, wine.getRegion());
        try
        {
            res = db.update(TABLE_NAME, values, _ID+"=?", new String[] {Long.toString(wine.getId())});
        }
        catch(SQLiteConstraintException exception)
        {
            return -1;
        }
        Log.d("USER", "COLUMNS COUNT : " + res);
        return res;
    }

    /**
     * Returns a cursor on all the wines of the library
     */
    public Cursor fetchAllWines() {
        SQLiteDatabase db = this.getReadableDatabase();

        final String[] COLUMNS = {_ID, COLUMN_NAME, COLUMN_WINE_REGION, COLUMN_LOC, COLUMN_CLIMATE, COLUMN_PLANTED_AREA};
        final String[] EMPTY = {};
        Cursor cursor;
        cursor = db.query(TABLE_NAME, COLUMNS, "", EMPTY, "", "", "", "");
        
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

     public void deleteWine(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();
        final String[] args = {String.valueOf(cursor.getInt(0))};
        db.delete(TABLE_NAME, "_id=?", args);
         Log.d("USER", "WINE DELETED");
         db.close();
    }

     public void populate() {
        addWine(new Wine("Châteauneuf-du-pape", "vallée du Rhône ", "Vaucluse", "méditerranéen", "3200"));
        addWine(new Wine("Arbois", "Jura", "Jura", "continental et montagnard", "812"));
        addWine(new Wine("Beaumes-de-Venise", "vallée du Rhône", "Vaucluse", "méditerranéen", "503"));
        addWine(new Wine("Begerac", "Sud-ouest", "Dordogne", "océanique dégradé", "6934"));
        addWine(new Wine("Côte-de-Brouilly", "Beaujolais", "Rhône", "océanique et continental", "320"));
        addWine(new Wine("Muscadet", "vallée de la Loire", "Loire-Atlantique", "océanique", "9000"));
        addWine(new Wine("Bandol", "Provence", "Var", "méditerranéen", "1500"));
        addWine(new Wine("Vouvray", "Indre-et-Loire", "Indre-et-Loire", "océanique dégradé", "2000"));
        addWine(new Wine("Ayze", "Savoie", "Haute-Savoie", "continental et montagnard", "20"));
        addWine(new Wine("Meursault", "Bourgogne", "Côte-d'Or", "océanique et continental", "395"));
        addWine(new Wine("Ventoux", "Vallée du Rhône", "Vaucluse", "méditerranéen", "7450"));

        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }

    public void clean()
    {
        this.getWritableDatabase().execSQL("DELETE FROM " + TABLE_NAME);
        this.getWritableDatabase().execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }

    public static Wine cursorToWine(Cursor cursor) {
        Wine wine = null;
        wine = new Wine(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5));
        return wine;
    }

    public Cursor idToCursor(long id)
    {
        Cursor current = fetchAllWines();
        while(!current.isAfterLast())
        {
            if(current.getLong(current.getColumnIndex(WineDbHelper._ID)) == id)
            {
                return current;
            }
            current.moveToNext();
        }
        return null;
    }
}
