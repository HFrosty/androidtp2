package com.example.androidtp2;

import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.TextView;

public class WineActivity extends AppCompatActivity {

    private Wine _wine;
    private boolean _newWine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        setContentView(R.layout.activity_wine);

        if(bundle.getBoolean("New") == true)
        {
            _wine = new Wine(bundle.getInt("Id"), "", "", "", "", "");
            _newWine = false;
        }
        else
        {
            _newWine = true;

            long wineId = bundle.getInt("Id");
            WineDbHelper db = new WineDbHelper(this);

            Cursor currentWine = db.fetchAllWines();
            boolean found = false;
            while(!found)
            {
                if(currentWine.getLong(0) == wineId)
                {
                    break;
                }
                currentWine.moveToNext();
            }

            _wine = db.cursorToWine(currentWine);
        }

        ( (TextView) findViewById(R.id.wineName) ).setText(_wine.getTitle());
        ( (TextView) findViewById(R.id.editWineRegion) ).setText(_wine.getRegion());
        ( (TextView) findViewById(R.id.editClimate) ).setText(_wine.getClimate());
        ( (TextView) findViewById(R.id.editLoc) ).setText(_wine.getLocalization());
        ( (TextView) findViewById(R.id.editPlantedArea) ).setText(_wine.getPlantedArea());
    }

    public void saveData(View view)
    {
        // Retrieve data
        _wine.setTitle(( (TextView) findViewById(R.id.wineName) ).getText().toString());
        _wine.setRegion(( (TextView) findViewById(R.id.editWineRegion) ).getText().toString());
        _wine.setClimate(( (TextView) findViewById(R.id.editClimate) ).getText().toString());
        _wine.setLocalization(( (TextView) findViewById(R.id.editLoc) ).getText().toString());
        _wine.setPlantedArea(( (TextView) findViewById(R.id.editPlantedArea) ).getText().toString());

        if(( (TextView) findViewById(R.id.wineName) ).getText().toString().isEmpty())
        {
            AlertDialog.Builder popup = new AlertDialog.Builder(WineActivity.this);
            popup.setTitle("Sauvegarde impossible");
            popup.setMessage("Vous devez ajouter un nom au vin.");
            popup.show();
            return;
        }

        // Put data in database
        if(!_newWine)
        {
            if(new WineDbHelper(this).addWine(_wine) == false)
            {
                AlertDialog.Builder popup = new AlertDialog.Builder(WineActivity.this);
                popup.setTitle("Sauvegarde impossible");
                popup.setMessage("Un vin portant le même nom existe déjà dans la base de données.");
                popup.show();
                return;
            }
            _newWine = true;
        }
        else
        {
            if(new WineDbHelper(this).updateWine(_wine) == -1)
            {
                AlertDialog.Builder popup = new AlertDialog.Builder(WineActivity.this);
                popup.setTitle("Sauvegarde impossible");
                popup.setMessage("Un vin portant le même nom existe déjà dans la base de données.");
                popup.show();
                return;
            }
        }

        Snackbar.make(view, "Données sauvegardées", Snackbar.LENGTH_LONG)
        .setAction("Action", null).show();
    }
}
